/*
 * owner.c
 *
 *  Created on: Oct 9, 2020
 *      Author: Shivam Jagtap
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void owner_area(user_t *u) {
	int choice;
	char password[20];
	do {
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. my profile \n4. display user\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:
				appoint_librarian();
				break;
			case 2:
				user_edit_by_id();
				break;
			case 3:
				printf("\n enter your password");
							scanf("%s",password);
							display_user_info(password);
				break;
			case 4:
				display_all_users();
				break;
		}
	}while (choice != 0);
}
void appoint_librarian()
{
	user_t u;
		user_accept(&u);
		strcpy(u.role,ROLE_LIBRARIAN);
		user_add(&u);
}
void display_all_users()
{
	FILE *fp;
			user_t u;
			// open the file for reading the data
			fp = fopen(USER_DB, "rb");
			if(fp == NULL) {
				perror("failed to open user file");
				return;
			}
			// read all user one by one
			while(fread(&u, sizeof(user_t), 1, fp) > 0) {
				user_display(&u);

			}
			// close file
			fclose(fp);

	}
