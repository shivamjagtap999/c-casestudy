/*
 * membre.c
 *
 *  Created on: Oct 9, 2020
 *      Author: Shivam Jagtap
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void member_area(user_t *u)
{
	user_t nu;
	int choice;
	char password[20];
	char name[100];
	do{
		printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. issued books\n4. Book Availability\n5.display info\nEnter choice:");
		scanf("%d", &choice);
				switch(choice) {
					case 1:
						printf("Enter book name: ");
						scanf("%s", name);
						book_find_by_name(name);
						break;

		case 2:
			 user_edit_by_id();

					break;
		case 3:
			display_issued_bookcopies(u->id);
					break;
		case 4:
			bookcopy_checkavail();
					break;
		case 5:
			printf("\n enter password");
			scanf("%s",password);
			display_user_info(password);
			break;
		}

	 }while(choice!=0);

}
void bookcopy_checkavail() {
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	// input book id
	printf("enter the book id: ");
	scanf("%d", &book_id);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}

	// read bookcopy records one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if book id is matching and status is available, count the copies
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0) {
		//	bookcopy_display(&bc);
			count++;
		}
	}
	// close book copies file
	fclose(fp);
	// print the message.
	printf("number of copies availables: %d\n", count);
}

